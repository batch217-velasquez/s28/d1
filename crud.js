// CRUD Operations
// C- Create
// R- Retrieve
// U - Update
// D -Delete


// SECTION - Create (Insert documents)

/*
	Syntax: db.collectionName.insertOne({object});
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
});

// Insert Many

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}
]);

// SECTION - READ (Finding documents)
/*
	db.collectionName.find();
	db.collectionName.find({field:value});

*/
// Will retrieve all documents
db.users.find();

// Will retrieve a specific document
db.users.find({firstName: "Stephen"});

// Finding documents with multiple parameter
// Syntax: db.collectionName.find({fieldA:valueA, fieldB:valueB});

db.users.find({lastName: "Armstrong", age: 82});

// SECTION - UPDATE (Updating documents)
// Syntax: // Syntax: db.collectionName.updateOne({criteria}, {$set: {field:value}});

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);

// Updating Multiple Documents

// Syntax: // Syntax: db.collectionName.updateOne({criteria}, {$set: {field:value}});

db.users.updateMany(
	{ department: "none"},
	{
		$set: { department: "HR"}
	}
);



db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

// Replace one

db.users.replaceOne(
	{ firstName: "Bill" },
	{
		firstName: "Mark"
	}	
);

// SECTION - DELETE (Deleting documents)

// Creating a document to delete
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

// Deleting a single document
// Syntax: db.collectionName.deleteOne({criteria});

db.users.deleteOne({
	firstName: "Test"
});

db.users.deleteMany({
	firstName: "Stephen"
});

// Query an embedded document
db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
});

// Query on nested field
db.users.find(
	{"contact.email": "janedoe@mail.com"}
);

// Querying an array with exact elements
db.users.find(
	{courses: ["CSS", "Javascript", "Python"]}
);

// // Querying an array without regard to order
db.users.find({courses: {$all: ["Javascript", "CSS", "Python"]}});

// Querying an embedded array
db.users.insertOne({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});

db.users.find({
	namearr:
	{
		namea: "juan"
	}
});